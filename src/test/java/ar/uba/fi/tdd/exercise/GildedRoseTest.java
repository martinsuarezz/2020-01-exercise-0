package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void foo() {
		StoredItem[] items = new StoredItem[] { new NormalItem("fixme", 0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat("fixme").isEqualTo(app.getItem(0).getName());
	}

	@Test
	public void normalItemDegradation(){
		StoredItem[] items = new StoredItem[] { new NormalItem("Normal Item", 1, 1) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.getItem(0).getQuality());
		assertThat(0).isEqualTo(app.getItem(0).getSellIn());
	}

	@Test
	public void negativeQualityNotAllowed(){
		StoredItem[] items = new StoredItem[] { new NormalItem("Normal Item", 1, 1) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(0).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void agedBrieQualityIncreases(){
		StoredItem[] items = new StoredItem[] { new AgedBrie("Aged Brie", 10, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(3).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void agedBrieQualityDoubleIncreasesAfterExpireDate(){
		StoredItem[] items = new StoredItem[] { new AgedBrie("Aged Brie", 0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(10).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void qualityCapsAt50(){
		StoredItem[] items = new StoredItem[] { new AgedBrie("Aged Brie", 10, 49) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(50).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void qualityOfSulfurasIsAlways80(){
		StoredItem[] items = new StoredItem[] { new LegendaryItem("Sulfuras, Hand of Ragnaros", 10, 80) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(80).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void normalItemDoubleDegradationAfterExpireDate(){
		StoredItem[] items = new StoredItem[] { new NormalItem("Normal Item", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(0).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void backstagePassesQualityIncreasesByOneIfSellinHigherThan10(){
		StoredItem[] items = new StoredItem[] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 15, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertThat(2).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void backstagePassesQualityIncreasesByTwoIfSellinBetween10and5(){
		StoredItem[] items = new StoredItem[] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 8, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertThat(4).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void backstagePassesQualityIncreasesByThreeIfSellinBetween5and0(){
		StoredItem[] items = new StoredItem[] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 5, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertThat(6).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void backstagePassesQualityIs0IfSellinIsNegative(){
		StoredItem[] items = new StoredItem[] { new BackstagePass("Backstage passes to a TAFKAL80ETC concert", 1, 20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertThat(0).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void addingANormalItem(){
		GildedRose app = new GildedRose(1);
		app.addNormalItem("Sword", 5, 10);
		assertThat("Sword").isEqualTo(app.getItem(0).getName());
		assertThat(5).isEqualTo(app.getItem(0).getSellIn());
		assertThat(10).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void gettingAnItemAdded(){
		GildedRose app = new GildedRose(1);
		app.addNormalItem("Sword", 5, 10);
		assertThat("Sword").isEqualTo(app.getItem(0).getName());
	}

	@Test
	public void conjuredItemDegradesTwiceAsFast(){
		StoredItem[] items = new StoredItem[] { new ConjuredItem("Conjured Shield", 5, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertThat(6).isEqualTo(app.getItem(0).getQuality());
	}

	@Test
	public void conjuredItemDegradesTwiceAsFastAfterSellIn(){
		StoredItem[] items = new StoredItem[] { new ConjuredItem("Conjured Shield", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertThat(2).isEqualTo(app.getItem(0).getQuality());
	}
}
