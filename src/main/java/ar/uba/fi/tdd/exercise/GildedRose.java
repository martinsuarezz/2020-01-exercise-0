
package ar.uba.fi.tdd.exercise;

class GildedRose {
    StoredItem[] items;
    int inventoryIndex = 0;

    public GildedRose(StoredItem[] _items) {
        items = _items;
    }

    public GildedRose(int capacity) {
        items = new StoredItem[capacity];
    }

    public void addNormalItem(String name, int sellIn, int quality){
        items[inventoryIndex] = new NormalItem(name, sellIn, quality);
        inventoryIndex += 1;
    }
    
    public StoredItem getItem(int position){
        return items[position];
    }

    // update the quality of the emements
    public void updateQuality() {
        // for each item
        for (int i = 0; i < items.length; i++) {
            items[i].updateQuality();
        }
    }
}
