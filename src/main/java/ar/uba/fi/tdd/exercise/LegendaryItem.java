package ar.uba.fi.tdd.exercise;

public class LegendaryItem extends StoredItem {

    private static final int QUALITY_CHANGE = 0;
    private static final int MINMUM_QUALITY = 80;
    private static final int MAXIMUM_QUALITY = 80;

    public LegendaryItem(String name, int sellIn, int quality){
        super(name, sellIn, quality, MINMUM_QUALITY, MAXIMUM_QUALITY);
    }

    public void updateQuality(){
        updateSellIn();
        changeQuality(QUALITY_CHANGE);
    }

}