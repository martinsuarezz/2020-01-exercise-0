package ar.uba.fi.tdd.exercise;

public class BackstagePass extends StoredItem {

    private static final int QUALITY_AFTER_SELLIN = 0;
    private static final int QUALITY_CHANGE_5_DAYS_BEFORE_SELLIN = 3;
    private static final int QUALITY_CHANGE_10_DAYS_BEFORE_SELLIN = 2;
    private static final int QUALITY_CHANGE = 1;
    private static final int MINMUM_QUALITY = 0;
    private static final int MAXIMUM_QUALITY = 50;

    public BackstagePass(String name, int sellIn, int quality){
        super(name, sellIn, quality, MINMUM_QUALITY, MAXIMUM_QUALITY);
    }

    public void updateQuality(){
        updateSellIn();
        int sellIn = getSellIn();

        if (sellIn < 0)
            setQuality(QUALITY_AFTER_SELLIN);
        else if (sellIn <= 5)
            changeQuality(QUALITY_CHANGE_5_DAYS_BEFORE_SELLIN);
        else if (sellIn <= 10)
            changeQuality(QUALITY_CHANGE_10_DAYS_BEFORE_SELLIN);
        else
            changeQuality(QUALITY_CHANGE);
    }

}