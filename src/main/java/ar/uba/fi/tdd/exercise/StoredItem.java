package ar.uba.fi.tdd.exercise;

public abstract class StoredItem {

    private Item item;
    private int minimumQuality;
    private int maximumQuality;

    protected StoredItem(String Name, int sellIn, int quality, int _minimumQuality, int _maximumQuality){
        item = new Item(Name, sellIn, quality);
        minimumQuality = _minimumQuality;
        maximumQuality = _maximumQuality;
        capQuality();
    }

    public int getQuality(){
        return item.quality;
    }

    public int getSellIn(){
        return item.sellIn;
    }

    public String getName(){
        return item.Name;
    }

    protected void changeQuality(int quantity){
        if (getSellIn() < 0) quantity = quantity * 2;
        setQuality(getQuality() + quantity);
        capQuality();
    }

    protected void updateSellIn(){
        item.sellIn = item.sellIn - 1;
    }

    protected void setQuality(int newQuality){
        item.quality = newQuality;
        capQuality();
    }

    public abstract void updateQuality();

    private void capQuality(){
        if (item.quality < minimumQuality)
            item.quality = minimumQuality;

        if (item.quality > maximumQuality)
            item.quality = maximumQuality;
    }

}