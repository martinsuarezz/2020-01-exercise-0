package ar.uba.fi.tdd.exercise;

public class NormalItem extends StoredItem {

    private static final int QUALITY_CHANGE = -1;
    private static final int MINMUM_QUALITY = 0;
    private static final int MAXIMUM_QUALITY = 50;

    public NormalItem(String name, int sellIn, int quality){
        super(name, sellIn, quality, MINMUM_QUALITY, MAXIMUM_QUALITY);
    }

    public void updateQuality(){
        updateSellIn();
        changeQuality(QUALITY_CHANGE);
    }

}